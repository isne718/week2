#pragma once

#ifndef LIST
#define LIST

#include <iostream>
using namespace std;

template<class T>
class Node {
public:
	T data;
	Node<T>* next, * prev;
	Node<T>()
	{
		next = prev = 0;
	}
	Node(T el, Node<T>* n = 0, Node<T>* p = 0)
	{
		data = el; next = n; prev = p;
	}
};

template<class T>
class List {
public:
	List() { head = tail = 0; }
	int isEmpty() { return head == 0; }
	~List();
	void pushToHead(T el);
	void pushToTail(T el);
	T popHead();
	T popTail();
	bool search(T el);
	void print();
private:
	Node<T>* head, * tail;
};

template<class T>
List<T>::~List() {
	for (Node<T>* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

template<class T>
void List<T>::pushToHead(T el)
{
	head = new Node<T>(el, head, 0);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{
		head->next->prev = head;
	}
}

template<class T>
void List<T>::pushToTail(T el)
{
	Node<T>* t = new Node<T>(el);
	tail->next = t;
	t->prev = tail;
	tail = t;
}

template<class T>
T List<T>::popHead()
{
	T el = head->data;
	Node<T>* tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
		head->prev = 0;
	}
	delete tmp;
	return el;
}

template<class T>
T List<T>::popTail()
{
	Node<T>* p = tail;
	T store = tail->data; //store value before delete
	tail = tail->prev; //point to another way
	delete p;
	tail->next = 0;
	return store;
}

template<class T>
bool List<T>::search(T el)
{
	Node<T>* f = head;

	while (f != 0) {
		if (f->data == el) {
			return true;
		}
		else {
			f = f->next;
		}
	}

	return false;
}

template<class T>
void List<T>::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node<T>* tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}

#endif